/*
 * ObjDetect.cpp
 * Author: Philip Karunakaran, Immanuel Rajkumar
 *
 * Program to detect colored objects using the openCV libraries
 */

#include <cv.h>
#include <highgui.h>
#include <ctime>

using namespace cv;

/*
 * Class for identifying object in a frame
 */
class ObjDetect {

private:
	double frameProcTime[5] = { 0, 0, 0, 0, 0 }; //Array to store time taken to process each frame in msec
	double avgFrameProcTime = 0; // Average processing time per frame;

public:

	/*
	 * Function to open default camera and process frames captured.
	 * Processing - Converting the captured frame to HSV scheme. Identifying colored object
	 * Color detected : yellow
	 */
	int identifyObject(void) {
		VideoCapture cap(0);

		if (!cap.isOpened()) {
			std::cout << "Unable to open the camera";
			return 0;
		}

		Mat frame, HSVFrame, processedFrame;
		namedWindow("Output", 1);

		clock_t start, end;
		double dur = 0;
		for (int i = 0;; i++) {
			dur = 0;
			start = clock();
			cap >> frame;
			//std::cout << "\nFrame Captured : " << i;
			cvtColor(frame, HSVFrame, CV_BGR2HSV);
			inRange(HSVFrame, Scalar(20, 100, 100), Scalar(30, 255, 255),
					processedFrame);
			imshow("Output", processedFrame);
			end = clock();
			dur = (double) (end - start) / CLOCKS_PER_SEC * 1000;

			/* Recording the processing time for the first 5 frames */
			if (i < 5)
				frameProcTime[i] = dur;

			/* The above processing time is without the waitkey time below */
			if (waitKey(30) >= 0)
				break;
		}

		return 0;
	}

	/*
	 * Function to report the processing time for the first 5 frame.
	 */
	int timeAnalysis(void) {
		for (int i = 0; i < 5; i++) {
			std::cout << "\nProcessing time for frame " << i + 1 << " : "
					<< frameProcTime[i] << " msec";
			avgFrameProcTime += frameProcTime[i];
		}
		std::cout << "\n Avg Processing time per frame in msec : "
				<< (avgFrameProcTime / 5);
		return 0;
	}
};

int main(void) {
	/* Driver object */
	ObjDetect driver;
	driver.identifyObject();
	driver.timeAnalysis();
	return 0;
}

